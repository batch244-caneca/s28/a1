//Single room
db.rooms.insertOne({
	name: "single",
	accommodates: 2,
	price: 1000,
	description: "A simple room with all the basic necessities",
	rooms_available: 10,
	isAvailable: false
});

//Multiple rooms
db.rooms.insertMany([
	{
		name: "double",
		accommodates: 3,
		price: 2000,
		description: "A room fit for a small family going on a vacation",
		rooms_available: 5,
		isAvailable: false
	},
	{
		name: "queen",
		accommodates: 4,
		price: 4000,
		description: "A room with a queen sized bed perfect for a simple getaway",
		rooms_available: 15,
		isAvailable: false
	}
]);

//Find room with name double
db.rooms.find({name: "double"});

//Update Queen room availability to zero
db.rooms.updateOne({name: "queen"},
	{	
		$set: {
			name: "queen",
			accommodates: 4,
			price: 4000,
			description: "A room with a queen sized bed perfect for a simple getaway",
			rooms_available: 0,
			isAvailable: false
		}
	}
);

//Delete rooms with zero availability
db.rooms.deleteMany({rooms_available: 0});